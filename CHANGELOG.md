# Change Log
All notable changes to this project will be documented in this file.

## Release 0.2.2
- Updated to file_lnk 0.2.2 and file_url 0.2.2

## Release 0.2.1
- Added 'lnks_base' and 'urls_base' params to leverage common.yaml as well as Role/Node yamls

## Release 0.2.0
- Changed parameter names to 'lnks' and 'urls'
- Updated to file_lnk 0.2.0 and file_url 0.2.0

## Release 0.1.9
- Initial release