require 'spec_helper'

describe 'windows_shortcuts' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      context 'with acceptable parameters' do
        let(:title) { 'windows_shortcuts_test01' }
        let(:params) do
          {
            lnks: [
              {
                file_name: 'Test01.lnk',
                parent_folder: 'C:\Users\Public\Desktop',
                target_path: 'C:\Windows\notepad.exe',
              },
            ],
            lnks_base: [
              {
                file_name: 'Test10.lnk',
                parent_folder: 'C:\Users\Public\Desktop',
                target_path: 'C:\Company\tool.exe',
              },
            ],
            urls: [
              {
                file_name: 'Test99.url',
                parent_folder: 'C:\Users\Public\Desktop',
                url: 'https://www.puppet.com',
              },
            ],
            urls_base: [
              {
                file_name: 'Test00.url',
                parent_folder: 'C:\Users\Public\Desktop',
                url: 'https://www.company.com',
              },
            ],
            log_output: true,
          }
        end

        it { is_expected.to compile }
        it { is_expected.to contain_notify("windows_shortcuts - number of LNK shortcuts: '1'") }
        it { is_expected.to contain_notify("windows_shortcuts - number of LNK base shortcuts: '1'") }
        it { is_expected.to contain_notify("windows_shortcuts - number of URL shortcuts: '1'") }
        it { is_expected.to contain_notify("windows_shortcuts - number of URL base shortcuts: '1'") }
        it { is_expected.to contain_file_lnk('Test01.lnk') }
        it { is_expected.to contain_file_lnk('Test10.lnk') }
        it { is_expected.to contain_file_url('Test99.url') }
        it { is_expected.to contain_file_url('Test00.url') }
      end
    end
  end
end
