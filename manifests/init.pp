# @summary Windows Shortcuts
#
# Manage Microsoft Windows shortcuts
#
# @param lnks
#   Array of .LNK shortcut hashes
#   Example: [
#   {
#     file_name: 'Example01',
#     parent_folder: 'C:\\Users\\Public\\Desktop',
#     target_path: 'C:\\Windows\\write.exe',
#     description: 'An example LNK shortcut',
#   },
#   {
#     file_name: 'Example02',
#     parent_folder: 'C:\\Users\\Public\\Desktop',
#     target_path: 'C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe',
#     description: 'Another example LNK shortcut',
#     target_arguments: 'http://its.uiowa.edu',
#   },
#   {
#     file_name: 'Example03',
#     parent_folder: 'C:\\Users\\Public\\Desktop',
#     set_run_as_administrator: true,
#     target_path: 'C:\\Windows\\notepad.exe',
#     target_arguments: 'C:\\Windows\\System32\\drivers\\etc\\hosts',
#     icon_file: 'C:\\Windows\\System32\\shell32.dll',
#     icon_index: 152,
#     description: 'One more example LNK shortcut',
#     window_style: '3',
#     working_directory: 'C:\\Windows',
#   },
#   {
#     file_name: 'Example04',
#     parent_folder: 'C:\\Users\\Public\\Desktop',
#     target_path: 'C:\\Windows',
#   },
#   {
#     file_name: 'Example99.lnk',
#     parent_folder: 'C:\\Users\\Public\\Desktop',
#     ensure: 'absent',
#   },
# ],
#
# @param lnks_base
#   Array of .LNK shortcut hashes that should be common across multiple nodes
#      Typically set within common.yaml, with $lnks param being set in Roles and/or Nodes
#   Example: [
#   {
#     file_name: 'Default01',
#     parent_folder: 'C:\\Users\\Public\\Desktop',
#     target_path: 'C:\\Company\\tool.exe',
#     description: 'An useful tool shortcut',
#   },
# ],
#
# @param urls
#   Array of .URL shortcut hashes
#   Example: [
#   {
#     file_name: 'Example01.url',
#     parent_folder: 'C:\\Users\\Public\\Desktop',
#     url: 'https://www.puppet.com',
#   },
#   {
#     file_name: 'Example02.url',
#     parent_folder: 'C:\\Users\\Public\\Desktop',
#     url: 'https://www.gitlab.com',
#     icon_file: 'C:\\Program Files\\Git\\git-cmd.exe',
#     icon_index: 0,
#   },
#   {
#     file_name: 'Example99.url',
#     parent_folder: 'C:\\Users\\Public\\Desktop',
#     ensure: 'absent',
#   },
# ],
#
# @param urls_base
#   Array of .URL shortcut hashes that should be common across multiple nodes
#      Typically set within common.yaml, with $urls param being set in Roles and/or Nodes
#   Example: [
#   {
#     file_name: 'Default01.url',
#     parent_folder: 'C:\\Users\\Public\\Desktop',
#     url: 'https://www.company.com',
#   },
# ],
#
# @param log_output
#   Boolean used to enable additional logging output
#   Example: true
#
# @example
#   include windows_shortcuts
class windows_shortcuts (
  Array[Hash] $lnks = [],
  Array[Hash] $lnks_base = [],
  Array[Hash] $urls = [],
  Array[Hash] $urls_base = [],
  Boolean $log_output = false,
) {
  if ($log_output) {
    notify { "${module_name} - number of LNK shortcuts: '${$lnks.length}'": }
    notify { "${module_name} - number of LNK base shortcuts: '${$lnks_base.length}'": }
    notify { "${module_name} - number of URL shortcuts: '${urls.length}'": }
    notify { "${module_name} - number of URL base shortcuts: '${urls_base.length}'": }
  }
  if ($lnks != []) {
    $lnks.each | Hash $lnks_hash | {
      file_lnk { $lnks_hash['file_name']:
        * => $lnks_hash,
      }
    }
  }
  if ($lnks_base != []) {
    $lnks_base.each | Hash $lnks_base_hash | {
      file_lnk { $lnks_base_hash['file_name']:
        * => $lnks_base_hash,
      }
    }
  }
  if ($urls != []) {
    $urls.each | Hash $url_hash | {
      file_url { $url_hash['file_name']:
        * => $url_hash,
      }
    }
  }
  if ($urls_base != []) {
    $urls_base.each | Hash $url_base_hash | {
      file_url { $url_base_hash['file_name']:
        * => $url_base_hash,
      }
    }
  }
}
