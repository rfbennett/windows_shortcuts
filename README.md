# Windows Shortcuts

Manage Microsoft Windows shortcuts.

## Table of Contents

1. [Description](#description)
2. [Usage - Configuration options and additional functionality](#usage)
3. [Limitations - OS compatibility, etc.](#limitations)
4. [Development - Guide for contributing to the module](#development)

## Description

This module allows you to manage .LNK and/or .URL shortcuts all in one place (as opposed to creating your own custom module with the file_lnk and file_url resources).

## Usage

Simply supply an array of shortcuts:
```
windows_shortcuts::shortcuts_lnk: [
  {
    file_name: 'Example01',
    parent_folder: 'C:\\Users\\Public\\Desktop',
    target_path: 'C:\\Windows\\write.exe',
    description: 'An example LNK shortcut',
  },
  {
    file_name: 'Example02',
    parent_folder: 'C:\\Users\\Public\\Desktop',
    target_path: 'C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe',
    description: 'Another example LNK shortcut',
    target_arguments: 'http://its.uiowa.edu',
  },
],
windows_shortcuts::shortcuts_url: [
  {
    file_name: 'Example01.url',
    parent_folder:> 'C:\\Users\\Public\\Desktop',
    url: 'https://www.puppet.com',
  },
  {
    file_name: 'Example02.url',
    parent_folder: 'C:\\Users\\Public\\Desktop',
    url: 'https://www.gitlab.com',
    icon_file: 'C:\\Program Files\\Git\\git-cmd.exe',
    icon_index: 0,
  },
]
```

## Limitations

1. This modules uses the following to generate the contents of the REFERENCE.md file:
  * `puppet strings generate --format markdown --out REFERENCE.md`
2. This module has been evaluated against the following:
  * Microsoft Windows Server 2022 (running PowerShell 5.1.20348.643)
  * Microsoft Windows Server 2019 (running PowerShell 5.1.17763.1971)
  * Microsoft Windows Server 2016 (running PowerShell 5.1.14393.4530)
  * Microsoft Windows 10 (running PowerShell 5.1.19041.1023)

## Development

Feedback and ideas are always welcome - please contact an Author (listed in metadata.json) to discuss your input, or feel free to simply [open an Issue](https://gitlab.com/rfbennett/windows_shortcuts/-/issues).

Command to apply this module locally (and try it out):
`puppet apply --modulepath="<PathToModuleParentFolders>" --execute "include <ModuleName>" --environment "<EnvironmentName>" --no-splay --verbose --debug`

For example: `puppet apply --modulepath="C:\ProgramData\PuppetLabs\code\environments\production\modules;c:\projects\forge" --execute "include windows_shortcuts" --environment "production" --no-splay`
